package main

import (
	"fmt"
	"os"
)

func main() {
	hello_from := os.Getenv("HELLO_FROM")

	if hello_from == "" {
		hello_from = "Nouvelle feature numéro 2"
	}

	fmt.Println("Hello from" + hello_from + "!")
	fmt.Println("La plateforme pour tester le suivi des KPI sous Gitlab")

	fmt.Println("")

	fmt.Print(GetTanuki(true))

	fmt.Println("")
	fmt.Println("lien du repo : https://gitlab.com/-/ide/project/jbily/kpi_test_gl_go")

}

func CreateTmpFile() {
	f, _ := os.Create("tanuki.tmp")
	defer f.Close()

	os.Chmod("tanuki.tmp", 0777)
}
